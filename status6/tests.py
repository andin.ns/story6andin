from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('/')
        self.assertFalse(response.status_code == 404)
    
    def test_url_using_status_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_statusform(self):
        status = StatusForm.objects.create(status = 'FINE')
        counting = StatusForm.objects.all().count()
        self.assertEqual(counting, 1)

    def test_save_a_POST_request(self):
        response = self.client.post('/',
         data={'status' : 'FINE'})
        counting = StatusForm.objects.all().count()
        self.assertEqual(counting, 1)

        self.assertEqual(response.status_code, 200)

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('FINE', html_response)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Story6Andin', self.browser.title)

        text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Hi, how are you feeling?', text)

        status = self.browser.find_element_by_id('id_status')
        status.send_keys('FINE')
        submit = self.browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)

