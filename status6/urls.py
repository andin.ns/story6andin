from django.urls import path
from . import views

app_name = 'status6'

urlpatterns = [
    path('', views.index, name='index'),
]
