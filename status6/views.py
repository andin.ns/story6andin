from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Form
from .models import StatusForm

# Create your views here.
response = {}

def index(request):
	result = StatusForm.objects.all()
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid():
			status = request.POST['status']
			create_stat = StatusForm.objects.create(status=status) 
			return render(request, 'index.html', {'form' : form, 'status':result})
	else:
		form = Form()
	return render(request, 'index.html', {'form' : form, 'status':result})
