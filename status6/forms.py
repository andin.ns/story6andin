from django import forms
from .models import StatusForm

class Form(forms.Form):
	error_messages = {
		'required': 'This field is required',
	}
	attrs = {
		'class': 'status-form-textarea',
		'type': 'text',
	}
    
	status = forms.CharField(label='Status', required=True, max_length=300, widget=forms.TextInput(attrs=attrs))