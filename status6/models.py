from django.db import models

# Create your models here.
class StatusForm(models.Model):
	status = models.CharField(max_length=300)
	date = models.DateTimeField(auto_now_add=True, editable=False)